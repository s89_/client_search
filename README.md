# __ClientSearch__

### __Intro__

The CustomerInfo model (`lib/customer_info.rb`) provides useful details on customers, from a given list of customers, in JSON format.  It relies on the Distance module (`lib/distance.rb`) which leans on the [`haversine`](https://rubygems.org/gems/haversine/versions/0.3.0) gem to do distance related calculations using the [Haversine formula](https://en.wikipedia.org/wiki/Haversine_formula).

### __Setup__

Run `bundle install` from the root directory to install all dependencies

### __Run the Test Suite__

Run the entire test suite using `rspec spec/`

### __Find Customers within 100km of Bristol__

To find the above, simply run the script `find_customers.rb` using:-

`ruby find_customers.rb`

The result will be output to the console.

### __Find the average value of customers within 200km of Bristol__

To find the above, simply run the script `find_average_value.rb` using:-

`ruby find_average_value.rb`

The result will be output to the console.

### __Extension__

To change the search criteria from Bristol, change the coordinates in the search scripts to your required location.  Actual data can be loaded into the `/data` folder but should be in the same format.