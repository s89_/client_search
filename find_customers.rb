# frozen_string_literal: true

require './lib/customer_info.rb'
require 'json'

json_file = File.open('data/people.json')
json_customers = JSON.load(json_file)
bristol_coordinates = [51.450167, -2.594678]
search_distance_km = 100

found_customers = CustomerInfo.customers_within_distance(bristol_coordinates, json_customers, search_distance_km)
reduced_customer_details = CustomerInfo.reduce_customer_details(found_customers)
sorted_customers = CustomerInfo.sort_customers_by_value(reduced_customer_details)

puts sorted_customers
