# frozen_string_literal: true

require './lib/customer_info.rb'
require 'json'

json_file = File.open('data/people.json')
json_customers = JSON.load(json_file)
bristol_coordinates = [51.450167, -2.594678]
search_distance_km = 200

average_value = CustomerInfo.average_value_within_distance(bristol_coordinates, json_customers, search_distance_km)

puts average_value
