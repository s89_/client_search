# frozen_string_literal: true

require './lib/distance.rb'

class CustomerInfo
  include Distance

  # Searches for customers within a certain distance from given coordinates
  def self.customers_within_distance(coordinates, customers, distance_km)
    filtered_customers = []
    customers.each do |customer|
      customer_coords = [customer['location']['latitude'], customer['location']['longitude']]
      if Distance.distance_between(customer_coords, coordinates) < distance_km
        filtered_customers << customer
      end
    end
    filtered_customers
  end

  # Sorts customers by descending value
  def self.sort_customers_by_value(customers)
    customers.sort_by { |hash| hash['value'].to_i }.reverse!
  end

  # Takes a list of customer details and outputs only their id, full name, value, and email
  def self.reduce_customer_details(customers)
    customers.each do |customer|
      customer.delete('address')
      customer.delete('location')
      customer.delete('company')
      customer.delete('country')
    end
    customers
  end

  # Gets an average value from a list of customers
  def self.average_value_within_distance(coordinates, customers, distance_km)
    filtered_customers = CustomerInfo.customers_within_distance(coordinates, customers, distance_km)
    value_array = filtered_customers.map{|customer| customer["value"].to_f}
    average_value = value_array.sum / value_array.size
    average_value.round(2)
  end
end
