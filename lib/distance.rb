# frozen_string_literal: true

require 'haversine'

module Distance
  # Returns the distance between two coordinates (float)
  def self.distance_between(location1, location2)
    loc1 = Distance.to_float_coords(location1)
    loc2 = Distance.to_float_coords(location2)
    distance = Haversine.distance(loc1[0], loc1[1], loc2[0], loc2[1])
    distance.to_km
  end

  # Converts string or int coordinates to float
  def self.to_float_coords(coords)
    fl_coords = coords
    fl_coords[0] = coords[0].to_f
    fl_coords[1] = coords[1].to_f
    fl_coords
  end
end
