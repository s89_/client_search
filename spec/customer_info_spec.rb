# frozen_string_literal: true

require './lib/customer_info.rb'
require 'json'

describe CustomerInfo do
  before(:each) do
    json_data = File.open('./spec/data/test_people.json')
    @json_customers = JSON.load(json_data)
    @bristol_coordinates = ['51.450167', '-2.594678']
  end

  it 'finds customers by distance' do
    customers_within_1km = CustomerInfo.customers_within_distance(@bristol_coordinates, @json_customers, 1)
    customers_within_3km = CustomerInfo.customers_within_distance(@bristol_coordinates, @json_customers, 3)
    customers_within_11km = CustomerInfo.customers_within_distance(@bristol_coordinates, @json_customers, 11)
    customers_within_10000km = CustomerInfo.customers_within_distance(@bristol_coordinates, @json_customers, 10_000)
    expect(customers_within_1km.count).to eq 0
    expect(customers_within_3km.count).to eq 1
    expect(customers_within_11km.count).to eq 2
    expect(customers_within_10000km.count).to eq 3
  end

  it 'sorts customers by value' do
    sorted_customers = CustomerInfo.sort_customers_by_value(@json_customers)
    expect(sorted_customers.first['value']).to eq '3749.61'
  end

  it 'reduces customer details' do
    reduced_customer_details = CustomerInfo.reduce_customer_details(@json_customers)
    expect(reduced_customer_details.first['address']).to eq nil
  end

  it 'gets the average value by distance' do
    avg_value_3km = CustomerInfo.average_value_within_distance(@bristol_coordinates, @json_customers, 3)
    avg_value_10000km = CustomerInfo.average_value_within_distance(@bristol_coordinates, @json_customers, 10000)
    expect(avg_value_10000km).to eq 3214.66
  end
end
