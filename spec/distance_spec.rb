# frozen_string_literal: true

require './lib/distance.rb'

describe Distance do
  it 'finds the distance between two coordinates' do
    bristol_coordinates = [51.450167, -2.594678]
    tokyo_coordinates = [35.685142, 139.778071] # Tokyo is approx 9655km away
    distance = Distance.distance_between(bristol_coordinates, tokyo_coordinates)
    expect(distance.ceil).to eq 9655
  end

  it 'converts string coordinates to float coordinates' do
    bristol_coordinates = ['51.450167', '-2.594678']
    expect(Distance.to_float_coords(bristol_coordinates)).to eq [51.450167, -2.594678]
  end
end
